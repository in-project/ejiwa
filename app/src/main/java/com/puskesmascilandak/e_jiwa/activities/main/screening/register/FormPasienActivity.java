package com.puskesmascilandak.e_jiwa.activities.main.screening.register;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.puskesmascilandak.e_jiwa.EJiwaPreference;
import com.puskesmascilandak.e_jiwa.R;
import com.puskesmascilandak.e_jiwa.Session;
import com.puskesmascilandak.e_jiwa.activities.main.screening.CheckUpActivity;
import com.puskesmascilandak.e_jiwa.model.Pasien;
import com.puskesmascilandak.e_jiwa.model.Petugas;
import com.puskesmascilandak.e_jiwa.model.User;
import com.puskesmascilandak.e_jiwa.responses.ApiEndPoint;
import com.puskesmascilandak.e_jiwa.responses.LastCheckedResponse;
import com.puskesmascilandak.e_jiwa.responses.LastCheckedResponse;
import com.puskesmascilandak.e_jiwa.service.ApiClient;
import com.puskesmascilandak.e_jiwa.service.PasienDbService;

import java.sql.SQLOutput;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormPasienActivity extends FormPersonActivity {

    //TextView periksaTv;
    int periksaKe;
    EJiwaPreference eJiwaPreference;

    public FormPasienActivity() {
        super(R.layout.activity_form_pasien);
    }

    @Override
    protected void initOnCreate() {
        super.initOnCreate();
        setTitle("Input Data Pasien");
        initUpNavigation();

        eJiwaPreference=new EJiwaPreference(this);
        inputPeriksa.setVisibility(View.VISIBLE);

        Button startBtn = findViewById(R.id.start_btn);
         //periksaTv = findViewById(R.id.tv_periksa);

        Session session = new Session(this);
        final User user = session.getUser();


        /*
        inputNoKtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence nik_pasien, int i, int i1, int i2) {
                    lastChecked(user.getPetugas().getNama(),String.valueOf(nik_pasien));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //System.out.println("Value "+getValueFrom(inputNoKtp));
            }

        });*/

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start(periksaKe);
            }
        });
    }


    private void initUpNavigation() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected boolean validateNoKtp() {
        if (!super.validateNoKtp()) return false;

        try {
            String noKtp = getValueFrom(inputNoKtp);
            PasienDbService service = new PasienDbService(this);
            Pasien pasien = service.findBy(noKtp);

            /*
            if (pasien != null) {
                showDialog("Gagal", "Sudah Ada Data Pasien Dengan No. KTP : "+noKtp);
                return false;
            }*/

            return true;
        } catch (SQLiteException e) {
            e.printStackTrace();
            showDialog("Gagal", "Tidak Dapat Mengakses Database");
            return false;
        }
    }

    private void start(int lastChecked) {


        if (!validateAllInput()) return;

        Pasien pasien = new Pasien();
        initData(pasien);
        pasien.setLastChecked(Integer.parseInt(inputPeriksa.getText().toString()));

        PasienDbService service = new PasienDbService(this);
        try {
            service.simpan(pasien);
            startCheckUpActivity(pasien);
        } catch (SQLiteException e) {
            Toast.makeText(this,"Gagal Menyimpan Data Pasien",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void startCheckUpActivity(Pasien pasien) {
        Session session = new Session(this);
        User user = session.getUser();

        if (user != null) {
            Petugas petugas = user.getPetugas();
            Intent intent = new Intent(this, CheckUpActivity.class);

            intent.putExtra("pasien", pasien);
            intent.putExtra("petugas", petugas);
            startActivity(intent);
            finish();
        }
    }

    public void lastChecked(String pemeriksa,String nik_pasien) {

        System.out.println(" Periksa "+nik_pasien);
        ApiEndPoint apiEndPoint = ApiClient.getClient().create(ApiEndPoint.class);
        Call<LastCheckedResponse> call = apiEndPoint.getLastChecked(pemeriksa,nik_pasien);

        call.enqueue(new Callback<LastCheckedResponse>() {
            @Override
            public void onResponse(Call<LastCheckedResponse> call, Response<LastCheckedResponse> response) {

                final LastCheckedResponse lastCheckedResponse = response.body();
                if (lastCheckedResponse.getStatus()) {
                    Log.d("Response Data ", "Total Data" + response);
                    if (lastCheckedResponse.getLastChecked().size()>0) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                periksaKe = Integer.parseInt(lastCheckedResponse.getLastChecked().get(0).getLastChecked()) + 1;
                                //periksaTv.setText("Pemeriksaan Ke "+periksaKe);
                                //eJiwaPreference.putLastChecked(EJiwaPreference.LAST_CHECKED,periksaKe);

                            }
                        });
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                System.out.println(" Masuk Sini ");
                                periksaKe = 1;
                                //eJiwaPreference.putLastChecked(EJiwaPreference.LAST_CHECKED,periksaKe);
                                //periksaTv.setText("Pemeriksaan Ke "+periksaKe);
                            }
                        });
                    }

                } else {
                    Log.d("StoreListFragment", "Data Null");
                }
            }

            @Override
            public void onFailure(Call<LastCheckedResponse> call, Throwable t) {
            }
        });

    }
}
